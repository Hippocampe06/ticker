import Section from './components/Section'
import HelloWorld from './HelloWorld'

const Main = () => (
  <main>
    <Section name="" component={HelloWorld} />
  </main>
)

export default Main
