import { useEffect, useState } from 'react'
import { TickerBoard } from 'react-ticker-board'

import "./HelloWorld.css";

import axios from 'axios';

// import departures from "./departures.jpg";


// import data from "./api/helipass";

const HelloWorld = () => {


  // axios.get('https://www.helipass.com:8765/ws_vols_du_jour.php',
  //   {
  //   headers: {
  //     "Access-Control-Allow-Origin": "*",
  //     'user': `helipass`,
  //     'password': `!çà6875412!çàà!ç` 
  //   }
  // })
  //   .then(function (response) {
  //     console.log(response);
  //   });


  let flights = [
    {
      "time": "2021-12-21 09:40:00",
      "produit": "PARIS-VERSAILLES",
      "status": "LANDING"
    },
    {
      "time": "2021-12-21 10:50:00",
      "produit": "LONDRES",
      "status": "ON GOING"
    },
    {
      "time": "2021-12-21 12:40:00",
      "produit": "TOULON",
      "status": "BOARDING"
    },
    {
      "time": "2021-12-21 16:00:00",
      "produit": "LONDRES",
      "status": "CANCELLED"
    },
    {
      "time": "2021-12-21 17:20:00",
      "produit": "PARIS-VERSAILLESSSSSSSSSSSSSSSxs",
      "status": "BOARDING"
    }
  ]



  const [messages, setMessages] = useState([]);
  const [newMessage, setNewMessage] = useState('')
  const [count, setCount] = useState(9)


  useEffect(() => {
    let flightToShow = []

    flights.forEach(flight => {
      let hour = flight.time.split(" ")[1];
      hour = hour.substr(0, hour.length - 3);


      let product = flight.produit;

      if (product.length < 18) {
        let toFill = 18 - product.length;
        let filling = " ".repeat(toFill);
        product = product + filling;
      } else {
        let toCut = product.length - 18;
        product = product.substring(0, product.length - toCut);
      }


      let status = flight.status;
      flightToShow.push(`${hour} ${product} ${status}`);
    });
    setMessages(flightToShow);
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault()
    if (newMessage) {
      setMessages([...messages, newMessage])
      setNewMessage('')
    }
  }

  return (
    <>
      <div className="logo">
      </div>

      <div className="titles">
        <div className="title-1">TIME</div>
        <div className="title-2">DESTINATION</div>
        <div className="title-3">STATUS</div>
      </div>

      <TickerBoard messages={messages} count={count} size={34} theme={'dark'} />
    </>
  )
}

export default HelloWorld
